from hamcrest import *
import StringIO
from pprint import pprint
from pms import *

def test_load():
	c = """id,absent,status,results,report,presentation-organization,presentation-skill,comments
012345,N,1,A,A,A,A,Present and Needs Attention
000000,Y,2,B+,B+,B+,C,Absent and Heading towards failure
999999,N,0,E,F,F,F,Largely on Track"""
	d = [{'absent': 'N',
  'comments': 'Present and Needs Attention',
  'id': '012345',
  'presentation-organization': 'A',
  'presentation-skill': 'A',
  'report': 'A',
  'results': 'A',
  'status': '1'},
 {'absent': 'Y',
  'comments': 'Absent and Heading towards failure',
  'id': '000000',
  'presentation-organization': 'B+',
  'presentation-skill': 'C',
  'report': 'B+',
  'results': 'B+',
  'status': '2'},
 {'absent': 'N',
  'comments': 'Largely on Track',
  'id': '999999',
  'presentation-organization': 'F',
  'presentation-skill': 'F',
  'report': 'F',
  'results': 'E',
  'status': '0'}]
	assert_that(load(StringIO.StringIO(c)), is_(d))