# PMS Uploader

PMS Uploader takes a CSV file and extracts the contents and uploads to the PMS system using Internet Explorer

## Prerequisites

Make sure you choose 32 or 64 bit accordingly

* [Python][1]
	* [setuptools][2]
	* [pip][3]
	* `pip install` [selenium][4]
* Internet Explorer
	* [IE Driver Server][5]
	
### Internet Explorer Notes

* ensure `Internet Options` => `Security` has **ALL** zones `Enable Protected Mode`
* ensure `Zoom` is set to 100%

## CSV File format

### Assessor File Format

id    |absent|midsem-status|results|report|presentation-organization|presentation-skill|comments
------|------|-------------|-------|------|-------------------------|------------------|------------------------------------
012345|N     |1            |A      |B+    |B                        |C+                |Present and Needs Attention
000000|Y     |2            |C      |D+    |F                        |A                 |Absent and Heading towards failure
999999|N     |0            |B+     |B     |C+                       |C                 |Largely on Track

### Supervisor File Format

id    |absent|midsem-status|results|report|project-management|personal-quality|comments
------|------|-------------|-------|------|------------------|----------------|------------------------------------
012345|N     |1            |A      |B+    |B                 |C+              |Present and Needs Attention
000000|Y     |2            |C      |D+    |F                 |A               |Absent and Heading towards failure
999999|N     |0            |B+     |B     |C+                |C               |Largely on Track

#### File Format Notes

The column headers cannot be changed, but additional columns can be added and will be ignored by the program

## Installation Instructions

Make sure the following files are all in the same directory

* `pms.py`
* `fyp.csv`
* `IEDriverServer.exe`

## Running Instructions

Execute `pms.py` using the console (to see if any errors occur)

### Running Notes

The default program logs in as `SOI Assessor`, edit main() to change to `SOI Supervisor` and also change `ASSESSOR_CATGEORIES` to `SUPERVISOR_CATGEORIES`

## Issues

Any problems with the program, raise an issue so I can address it. Thanks.

[1]: http://www.python.org/download/
[2]: http://www.lfd.uci.edu/~gohlke/pythonlibs/#setuptools
[3]: http://www.lfd.uci.edu/~gohlke/pythonlibs/#pip
[4]: http://pypi.python.org/pypi/selenium
[5]: http://code.google.com/p/selenium/downloads/list