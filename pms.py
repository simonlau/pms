import logging
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def login(driver, wait, role_text='SOI-RoleAssessor'):
    logging.info('Loading login page')
    driver.get("http://myrp.sg/projectmodule/Login.aspx")
    wait.until(EC.title_is('Project Module System -- Republic Polytechnic'))

    SEMESTER_ID = 'ctl00_CPH_CentreMid_ddlActiveSemesters'
    semester = Select(driver.find_element_by_id(SEMESTER_ID))
    semester.select_by_visible_text('2013/2014, 1')
    logging.info('Selected semester')

    ROLE_ID = 'ctl00_CPH_CentreMid_ddlSchoolAndRole'
    role = Select(driver.find_element_by_id(ROLE_ID))
    role.select_by_visible_text(role_text)
    logging.info('Selected role')

    PROCEED_ID = 'ctl00_CPH_CentreMid_lnkProceed'
    driver.find_element_by_id(PROCEED_ID).click()
    logging.info('Click Proceed')
    wait.until(EC.title_is('Project Module System'))
    logging.info('Main page loaded')

def load(csvfile):
    import csv
    reader = csv.DictReader(csvfile)
    data = [row for row in reader]
    return data

def update(driver, student, wait):
    url = "http://myrp.sg/ProjectModule/Common/ProjectAssessment/Assessment.aspx?studid={0}".format(student['id'])
    logging.info('Load student {0}'.format(student['id']))
    driver.get(url)

    COMMENTS_ID = 'ctl00_CPH_CentreMid_commenttxtbox'
    comments = wait.until(EC.element_to_be_clickable((By.ID, COMMENTS_ID)))
    comments.clear()
    comments.send_keys(student['comments'])
    logging.info('Key in comments')

    if student['absent'].upper() == 'Y':
        ABSENT_ID = 'ctl00_CPH_CentreMid_chkbox'
        driver.find_element_by_id(ABSENT_ID).click()
        logging.info('Click absent box')

    STATUS_ID = 'ctl00_CPH_CentreMid_gv_ctl02_rblOption_{0}'
    driver.find_element_by_id(STATUS_ID.format(student['midsem-status'])).click()
    logging.info('Update status as {0}'.format(student['midsem-status']))
    
    UPDATE_ID = 'ctl00_CPH_CentreMid_btnupdate'
    driver.find_element_by_id(UPDATE_ID).click()
    logging.info('Click Update')

    wait.until(EC.alert_is_present())
    alert = driver.switch_to_alert()
    alert.dismiss()
    logging.info('Dismiss alert')

def final_update(driver, student, wait, categories):
    url = "http://myrp.sg/ProjectModule/Common/ProjectAssessment/Assessment.aspx?studid={0}".format(student['id'])
    logging.info('Load student {0}'.format(student['id']))
    driver.get(url)

    COMMENTS_ID = 'ctl00_CPH_CentreMid_commenttxtbox'
    comments = wait.until(EC.element_to_be_clickable((By.ID, COMMENTS_ID)))
    comments.clear()
    comments.send_keys(student['comments'])
    logging.info('Key in comments')

    if student['absent'].upper() == 'Y':
        ABSENT_ID = 'ctl00_CPH_CentreMid_chkbox'
        driver.find_element_by_id(ABSENT_ID).click()
        logging.info('Click absent box')

    FYP_GRADES = ['A', 'B+', 'B', 'C+', 'C', 'D+', 'D', 'F']
    GRADES = dict(zip(FYP_GRADES, xrange(8)))
    CATEGORY_ID = 'ctl00_CPH_CentreMid_gv_ctl0{0}_rblOption_{1}'
    for num, category in enumerate(categories):
        grade = GRADES[student[category]]
        grade_element = wait.until(EC.element_to_be_clickable((By.ID, CATEGORY_ID.format(num + 2, grade))))
        grade_element.click()
        logging.info('Update {0} as {1}'.format(category, student[category]))
    
    UPDATE_ID = 'ctl00_CPH_CentreMid_btnupdate'
    driver.find_element_by_id(UPDATE_ID).click()
    logging.info('Click Update')

    wait.until(EC.alert_is_present())
    alert = driver.switch_to_alert()
    alert.dismiss()
    logging.info('Dismiss alert')

def publish(driver, wait):
    driver.get('http://myrp.sg/ProjectModule/Common/ProjectAssessment/FinalAssessment.aspx')
    logging.info('Load projects page')

    PROJECTS_ID = 'ctl00_CPH_CentreMid_ddlProject'
    projects = Select(driver.find_element_by_id(PROJECTS_ID))
    for project in projects.options:
        project.click()
        logging.info("Loading project {}".format(project))

        projects.submit()
        wait.until(EC.alert_is_present())
        alert = driver.switch_to_alert()
        alert.accept()
        logging.info('Accept alert')


def main(csvfilename):
    with open(csvfilename) as csvfile:
        students = load(csvfile)
    driver = webdriver.Ie('./IEDriverServer.exe')
    wait = WebDriverWait(driver, 60)
    
    login(driver, wait, role_text='SOI-RoleAssessor') # SOI-RoleSupervisor
    ASSESSOR_CATGEORIES = ['results', 'report', 'presentation-organization', 'presentation-skill']
    SUPERVISOR_CATGEORIES = ['results', 'report', 'project-management', 'personal-quality']
    for student in students:
        final_update(driver, student, wait, ASSESSOR_CATGEORIES)
    #publish(driver, wait)

    driver.close()

if __name__ == '__main__':
    import time
    t0 = time.time()
    logging.basicConfig(level=logging.INFO)
    main('fyp.csv')
    t1 = time.time()
    logging.info("Saved:{}".format(t1 - t0))